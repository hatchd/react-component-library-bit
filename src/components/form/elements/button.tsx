import React, { HTMLAttributes, memo } from 'react';

export interface Button extends HTMLAttributes<HTMLElement> {
    /** Content of the button */
    children: any,
    /** Button type i.e. primary or secondary */
    type: string,
    /** Button color */
    color?: string
}

/** This is a button component */
const Button: React.FC<Button> = memo(({
    children,
    type,
    color
}) => {
    return <button className={type}>{children}</button>;
});

export default Button;
