import React, { HTMLAttributes, memo } from 'react';

export interface Input extends HTMLAttributes<HTMLElement> {
    /** Input value */
    defaultValue?: string
}

const Input: React.FC<Input> = memo(({
    defaultValue,
}) => {
    return (
        <input type="text" defaultValue={defaultValue} />
    )
});

export default Input;