import React from 'react';

const Select = () => {
    return (
        <select>
            <option>Option 1</option>
            <option>Option 2</option>
            <option>Option 3</option>
        </select>
    )
}

export default Select;