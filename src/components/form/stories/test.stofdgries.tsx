import React from 'react';
import MyComponent from './test';

export default {
    title: 'Path/To/MyComponent',
    component: MyComponent,
}

export const Basic = () => <MyComponent />;