# React Component Library

This project skeleton was created to help people get started with creating their own React component library using:
- [Rollup](https://github.com/rollup/rollup)
- [Sass](https://sass-lang.com/)
- [TypeScript](https://www.typescriptlang.org/)

It also features:
- :white_check_mark: [Storybook](https://storybook.js.org/) to help you create and show off your components
- :white_check_mark: [Jest](https://jestjs.io/) and [Enzyme](https://airbnb.io/enzyme/) enable testing of the components

[**Read my blog post about why and how I created this project skeleton ▸**](https://blog.harveydelaney.com/creating-your-own-react-component-library/)

## Development
### Testing
`npm run test` or `yarn test`

### Building
`npm run build` or `yarn build`

### Storybook
`npm run storybook` or `yarn storybook`

### Publishing
`npm run publish`

## Component Usage
Let's say you created a public npm library called `react-component-library` with the `Button` component created in this repository.

Usage of the component (once published to the registry and then installed into another project) will be:

```
import React from "react";
import { Button } from "react-component-library";

const TestApp = () => (
    <div className="app-container>
        <Button color="primary" type="solid" />
    </div>
);

export TestApp;
```

## Bit.dev

Before you can import or export a component from bit.dev, ensure you are logged in

1. Enter `bit login` in a terminal window. This will open a new browser window to log into bit.dev
2. Enter login details. If already logged in you will see a message 'already logged in'

To export a new component to the Hatchd Bit.dev repository:

1. Create a new react component within `src/components`
2. In a terminal window enter `bit add 'src/components/<component name>'`
3. Enter `bit status` to check for staged components
4. Tag all staged components with a new version `bit tag --all 0.0.1`
5. Export tagged components to the Hatchd bit.dev repo `bit export hatch.react-component-library`

You can install individual components into your app from bit.dev by running the following command:

`npm i @bit/hatchd.react-component-library.<component-name>`