import { addons } from '@storybook/addons';
import hatchdTheme from './hatchdTheme';

addons.setConfig({
  theme: hatchdTheme,
});