import { create } from '@storybook/theming/create';
import imageFile from '../src/assets/hatchd-logo.png';

export default create({
  base: 'light',

//   colorPrimary: 'rgb(239, 66, 54)',
//   colorSecondary: 'black',

//   // UI
//   appBg: 'white',
//   appContentBg: 'white',
//   appBorderColor: 'grey',
//   appBorderRadius: 4,

//   // Typography
//   fontBase: '"Open Sans", sans-serif',
//   fontCode: 'monospace',

//   // Text colors
//   textColor: 'black',
//   textInverseColor: 'rgba(255,255,255,0.9)',

//   // Toolbar default and active colors
//   barTextColor: 'white',
//   barSelectedColor: 'white',
//   barBg: 'rgb(239, 66, 54)',

//   // Form colors
//   inputBg: 'white',
//   inputBorder: 'silver',
//   inputTextColor: 'black',
//   inputBorderRadius: 4,

  brandTitle: 'Hatchd component library',
  brandUrl: 'https://hatchd.com.au',
  // brandImage: imageFile,
  brandImage: 'https://placehold.it/350x150',
});