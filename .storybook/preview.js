import { addParameters, addDecorator } from '@storybook/react';
import { withA11y } from '@storybook/addon-a11y';
import { withKnobs } from '@storybook/addon-knobs';

addDecorator(withKnobs);
addDecorator(withA11y);
addParameters({
  options: {
    storySort: (a, b) => {
        if (a[1].id.toLowerCase().includes('start-here') && b[1].id.toLowerCase().includes('start-here')) {
            return a[1].id.split('--')[0].localeCompare(b[1].id.split('--')[0]);
          } else if (a[1].id.toLowerCase().includes('start-here')) {
            return -1;
          } else if (b[1].id.toLowerCase().includes('start-here')) {
            return 1;
          } else {
            return a[1].id.split('--')[0].localeCompare(b[1].id.split('--')[0]);
          }
    }
  },
});