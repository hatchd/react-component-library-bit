import React, { memo } from 'react';
const Button = memo(({ children, type, color }) => {
    console.log(color);
    return React.createElement("button", { className: type }, children);
});
export default Button;
