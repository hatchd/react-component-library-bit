import React, { HTMLAttributes } from 'react';
export interface Button extends HTMLAttributes<HTMLElement> {
    children: any;
    type: string;
    color?: string;
}
declare const Button: React.FC<Button>;
export default Button;
