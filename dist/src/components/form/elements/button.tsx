import React from 'react';

export type Button = {
    children: any,
    type: string,
    color?: string
}

/** This is a button component */
export const Button: React.FC<Button> = ({
    children,
    type,
    color
}) => {
    console.log(color);
    return <button className={type}>{children}</button>;
};

// export default Button;
